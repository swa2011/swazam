package at.ac.tuwien.infosys.shazam.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;

public class SearchRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	private static int totalSearchRequests = 0;
	private int id;
	private String shift;
	private LinkedList<String> subfingerprint;
	private Date date;
	private String fromUser;
	private String responseHost;
	private int responsePort;
	private String foundBy;
	private boolean found;
	private String result;
	
	/**
	 * Constructor
	 * @param shift
	 * @param subfingerprint a linked list of subfingerprints
	 * @param date
	 * @param fromUser the user that started this search
	 * @param responseHost the requesting users IP
	 * @param responsePort the requesting users port
	 */
	public SearchRequest(String shift, LinkedList<String> subfingerprint, Date date, String fromUser, String responseHost, int responsePort) {
		this.id = totalSearchRequests;
		this.shift = shift;
		this.date = date;
		this.fromUser = fromUser;
		this.responseHost = responseHost;
		this.responsePort = responsePort;
		this.subfingerprint = subfingerprint;
		totalSearchRequests++;
	}

	public String getShift() {
		return shift;
	}

	public String getFoundBy() {
		return foundBy;
	}

	public void setFoundBy(String foundBy) {
		this.foundBy = foundBy;
	}

	public boolean isFound() {
		return found;
	}

	public void setFound(boolean found) {
		this.found = found;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getResponseHost() {
		return responseHost;
	}

	public int getResponsePort() {
		return responsePort;
	}
	
	public String getFromUser() {
		return fromUser;
	}

	public LinkedList<String> getSubfingerprint() {
		return subfingerprint;
	}

	public void setSubfingerprint(LinkedList<String> subfingerprint) {
		this.subfingerprint = subfingerprint;
	}

	public int getId() {
		return id;
	}
	
	public Date getDate() {
		return date;
	}
	
	// needed for de/serialising static counter
	private void writeObject(ObjectOutputStream oos) throws IOException 
	{
		oos.defaultWriteObject();
		oos.writeObject(new Integer(totalSearchRequests));
	}

	private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException 
	{
		ois.defaultReadObject();
		totalSearchRequests = (Integer)ois.readObject();
	}
	
	/**
	 * returns information about this searchrequest
	 */
	@Override
	public String toString() {
		String s = "";
		
		s += this.getId() + "\t\t" + this.fromUser + "\t\t" +  this.found + "\t\t" + this.result + "\n";
		
		return s;
	}
}

