package at.ac.tuwien.infosys.shazam.server;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.logging.Logger;

// stores account information and provides access to them
public class AccountDatabase implements ServerDatabase, Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private transient DatabaseSerializer dbs;
	private transient Logger log;
	private LinkedList<Account> accounts = new LinkedList<Account>();

	/**
	 * Constructor
	 */
	public AccountDatabase() {
		setLogger();
		setSerializer();
	}
	
	public void setLogger() {
		log = Logger.getAnonymousLogger();
		log.info("PeerDB up and running");
	}
	
	public void setSerializer() {
		dbs = new DatabaseSerializer();
	}
	
	/**
	 * returns the size of the database
	 */
	public int size() {
		return accounts.size();
	}
	
	/**
	 * add an account to the db
	 * @param a an account
	 */
	public synchronized void add(Account a) {		

		if (find(a.getUsername()) != null) {
			log.info("account already in database, aborting");
			return;
		}
		
		accounts.add(a);
		log.info("added account to database:\n" + a.toString());
		
		save();
	}
	
	/**
	 * find account by user/pass
	 * @param userName
	 * @param password
	 * @return an account or null if not found
	 */
	public Account find(String userName, String password) {
		for (Account a : accounts) {
			if (a.getUsername().equals(userName)) {
				if (a.checkPassword(password))
					return a;
			}
		}
		return null;
	}

	/**
	 * find an account by username
	 * @param userName
	 * @return an account or null if not found
	 */
	public Account find(String userName) {
		for (Account a : accounts) {
			if (a.getUsername().equals(userName)) {
				return a;
			}
		}
		return null;
	}
	
	/**
	 * returns a string with information about all accounts
	 */
	@Override
	public String toString() {
		String s = "";
		
		for (Account a : accounts) {
			s += a.toString();
		}
		
		return s;
	}

	public LinkedList<Account> getAccounts() {
		return accounts;
	}
	
	/**
	 * serialize db to file
	 */
	public void save() {
		dbs.serialize(this, "account.db");
	}
}
