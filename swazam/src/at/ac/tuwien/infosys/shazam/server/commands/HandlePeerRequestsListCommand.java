package at.ac.tuwien.infosys.shazam.server.commands;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import at.ac.tuwien.infosys.shazam.communication.listener.Command;
import at.ac.tuwien.infosys.shazam.server.Peer;
import at.ac.tuwien.infosys.shazam.server.PeerDatabase;
import at.ac.tuwien.infosys.shazam.server.Server;

public class HandlePeerRequestsListCommand implements Command {
	Logger log = Logger.getAnonymousLogger();
	Server server = Server.getInstance();
	PeerDatabase peerDB = Server.getPeerDB();

	/**
	 * peer requests list of peers (3:192.168.1.2:4323)
	 */
	@Override
	public void execute(String[] msg, String fromHost, int fromPort) {
		String host = msg[1].split(":")[0];
		String port = msg[1].split(":")[1];
		List<Peer> peerList = new LinkedList<Peer>();
		String peerListMessage;
		
		// find peer by ip:port
		Peer requestingPeer = peerDB.find(host, port);
		
		if (requestingPeer == null) {
			log.warning("requesting peer not in database, aborting...");
			return;
		}
		
		// get list of peers
		peerList = peerDB.getPeers(Server.NUMOFSEEDPEERS);

		// convert ist to msg string
		peerListMessage = "2;";

		for (Peer p : peerList) {
			peerListMessage += p.getIp();
			peerListMessage += ":";
			peerListMessage += p.getPort();
			peerListMessage += ";";
		}

		// send the list
		requestingPeer.sendMessage(peerListMessage);		
	}
}
