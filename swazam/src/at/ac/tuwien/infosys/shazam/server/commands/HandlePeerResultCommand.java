package at.ac.tuwien.infosys.shazam.server.commands;

import java.util.logging.Logger;

import at.ac.tuwien.infosys.shazam.communication.listener.Command;
import at.ac.tuwien.infosys.shazam.server.Account;
import at.ac.tuwien.infosys.shazam.server.AccountDatabase;
import at.ac.tuwien.infosys.shazam.server.PeerDatabase;
import at.ac.tuwien.infosys.shazam.server.SearchDatabase;
import at.ac.tuwien.infosys.shazam.server.SearchRequest;
import at.ac.tuwien.infosys.shazam.server.Server;

public class HandlePeerResultCommand implements Command {
	private Logger log = Logger.getAnonymousLogger();
	private SearchDatabase searchDB = Server.getSearchDB();
	private AccountDatabase accDB = Server.getAccDB();
	private PeerDatabase peerDB = Server.getPeerDB();
	
	/**
	 * peer sent result for a search, format: 6;user;pass;ID;RESULT
	 */
	@Override
	public void execute(String[] msg, String fromHost, int fromPort) {
		Account resultAccount;
		SearchRequest req;
		
		req = searchDB.find(Integer.valueOf(msg[3]));		
		resultAccount = accDB.find(msg[1]);

		// write result to search db
		req.setFound(true);
		req.setFoundBy(msg[1]);
		req.setResult(msg[4]);		
		log.info("Result for search " + req.getId() + ": " + req.getResult() + " written to database.");
		
		// adjust coins in account db
		//requestAccount.setCoins(requestAccount.getCoins() - 1);
		resultAccount.setCoins(resultAccount.getCoins() + 1);
		// increment peers songsFound counter
		peerDB.find(resultAccount).setSongsFound(peerDB.find(resultAccount).getSongsFound() + 1);
		
		searchDB.save();
		accDB.save();
		peerDB.save();

		log.info(searchDB.toString());
		
		// TODO try to notify client (but we don't have a message for that at the moment,
		// so the client polls for a history every so often, which also gives him the results)
		
	}
}