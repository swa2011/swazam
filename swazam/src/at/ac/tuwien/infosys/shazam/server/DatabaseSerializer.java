package at.ac.tuwien.infosys.shazam.server;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.logging.Logger;

public class DatabaseSerializer {

	/**
	 * write a database to disk
	 * @param db the database to serialize
	 * @param file the file to store the serialized database
	 */
	public void serialize(ServerDatabase db, String file) {
		ObjectOutput out = null;
		
		try {
			out = new ObjectOutputStream(new FileOutputStream(file));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		 try {
			out.writeObject(db);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		 try {
			out.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * restore a database from a file
	 * @param file the file storing the serialized database
	 * @return the deserialised database
	 */
	public ServerDatabase deserialize(String file) {
		ServerDatabase db = null;
		ObjectInput in = null;
		
		try {
			in = new ObjectInputStream(new FileInputStream(file));
		} catch (FileNotFoundException e2) {
			Logger.getAnonymousLogger().warning("File " + file + " not found (no need to worry, maybe there never was a database to serialize yet :-) )");
			return null;
		} catch (IOException e2) {
			e2.printStackTrace();
			return null;
		}
		try {
			db = (ServerDatabase)in.readObject();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return db;
	}	
}
