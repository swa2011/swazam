package at.ac.tuwien.infosys.shazam.server;

public interface ServerDatabase {
	public int size();
	public void save();
}

