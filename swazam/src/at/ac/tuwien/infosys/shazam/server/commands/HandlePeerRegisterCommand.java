package at.ac.tuwien.infosys.shazam.server.commands;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import at.ac.tuwien.infosys.shazam.communication.listener.Command;
import at.ac.tuwien.infosys.shazam.server.Account;
import at.ac.tuwien.infosys.shazam.server.AccountDatabase;
import at.ac.tuwien.infosys.shazam.server.Peer;
import at.ac.tuwien.infosys.shazam.server.PeerDatabase;
import at.ac.tuwien.infosys.shazam.server.Server;

public class HandlePeerRegisterCommand implements Command {
	private Logger log = Logger.getAnonymousLogger();
	private PeerDatabase peerDB = Server.getPeerDB();
	private AccountDatabase accDB = Server.getAccDB();
	
	/**
	 * Handle initial peer message
	 */
	@Override
	public void execute(String[] msg, String fromHost, int fromPort) {
		List<Peer> peerList = new LinkedList<Peer>();
		String peerListMessage;
		Account account;

		account = accDB.find(msg[1]);
		if (account == null) {
			account = new Account(msg[1], msg[2]);
			accDB.add(account);
			log.warning("Could not find account for user " + msg[1] + " creating it");
		}
		
		// add peer to peer db (1;user;pass;192.168.1.1:5432)
		Peer newPeer = new Peer(account, msg[3].split(":")[0], msg[3].split(":")[1]);
		log.info(newPeer.getIp() + " " + newPeer.getPort() + " " + newPeer.getId());
		peerDB.add(newPeer);

		// get peer (sub)list
		peerList = peerDB.getPeers(Server.PEERLISTSIZE);
				
		// send list to peer (2;192.168.1.2:4325;192.168.1.21:5432;...)
		peerListMessage = "2;";
		
		for (Peer p : peerList) {
			peerListMessage += p.getIp();
			peerListMessage += ":";
			peerListMessage += p.getPort();
			peerListMessage += ";";
		}
		
		newPeer.sendMessage(peerListMessage);
	}
}
