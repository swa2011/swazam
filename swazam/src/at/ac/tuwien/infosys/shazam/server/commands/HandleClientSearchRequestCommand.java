package at.ac.tuwien.infosys.shazam.server.commands;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import at.ac.tuwien.infosys.shazam.communication.dispatcher.MessageDispatcher;
import at.ac.tuwien.infosys.shazam.communication.listener.Command;
import at.ac.tuwien.infosys.shazam.server.Account;
import at.ac.tuwien.infosys.shazam.server.AccountDatabase;
import at.ac.tuwien.infosys.shazam.server.Peer;
import at.ac.tuwien.infosys.shazam.server.PeerDatabase;
import at.ac.tuwien.infosys.shazam.server.SearchDatabase;
import at.ac.tuwien.infosys.shazam.server.SearchRequest;
import at.ac.tuwien.infosys.shazam.server.Server;

public class HandleClientSearchRequestCommand implements Command {
	Logger log = Logger.getAnonymousLogger();
	Server server = Server.getInstance();
	PeerDatabase peerDB = Server.getPeerDB();
	SearchDatabase searchDB = Server.getSearchDB();
	AccountDatabase accDB = Server.getAccDB();
	/**
	 * client sends a search request of format: 10;USER;PASS;shift;subfingerprint;subfingerprint;...;...
	 */
	@Override
	public void execute(String[] msg, String fromHost, int fromPort) {
		String searchRequestMessage;
		List<Peer> peerList;
		LinkedList<String> subfingerprints = new LinkedList<String>();
		Account clientAccount = accDB.find(msg[1]);
		
		// check if client is already in account database
		if (clientAccount == null) { 
			// create new account and add to database
			clientAccount = new Account(msg[1], msg[2]);
			accDB.add(clientAccount);
		}
		
		// check password
		if (!clientAccount.checkPassword(msg[2])) {
			log.info("wrong password, sorry...");
			return;
		}
		
		// check coins and decrement if enough
		if (clientAccount.getCoins() > 0) {
			clientAccount.setCoins(clientAccount.getCoins() - 1);
			accDB.save();
		}
		else {
			log.info("not enough, sorry...");
			return;
		}
		
		for (int i = 4; i < msg.length; i++) {
			subfingerprints.add(msg[i]);
		}

		// add search request to search db
		SearchRequest currentSearchRequest = new SearchRequest(msg[3], subfingerprints, new Date(), msg[1], fromHost, fromPort); 
		searchDB.add(currentSearchRequest);

		// construct searchRequestMessage 5;3;SERVERIP:SERVERPORT;ID;shift;subFingerprint1;subFingerPrint2;.....
		searchRequestMessage = 
				"5;" + Server.MAXHOPS + ";" + Server.SERVER_IP + ":" + Server.SERVER_PORT + ";" + currentSearchRequest.getId() + ";" + msg[3] + ";";		

		for (int i = 4; i < msg.length; i++) {
			searchRequestMessage += msg[i] + ";";
		}
		
		// get initial peers		
		peerList = peerDB.getPeers(Server.NUMOFSEEDPEERS);

		// send request to initial peers
		for (Peer p : peerList) {
			p.sendMessage(searchRequestMessage);
		}
			
		// ACK to client
		new MessageDispatcher().dispatchMessage(fromHost, Server.CLIENTPORT, "11;" + currentSearchRequest.getId() + ";");
	}
}
