package at.ac.tuwien.infosys.shazam.server.commands;

import java.util.LinkedList;
import java.util.logging.Logger;

import at.ac.tuwien.infosys.shazam.communication.dispatcher.MessageDispatcher;
import at.ac.tuwien.infosys.shazam.communication.listener.Command;
import at.ac.tuwien.infosys.shazam.server.SearchDatabase;
import at.ac.tuwien.infosys.shazam.server.SearchRequest;
import at.ac.tuwien.infosys.shazam.server.Server;

public class HandleClientRequestsHistoryCommand implements Command {
	private Logger log = Logger.getAnonymousLogger();
	private static SearchDatabase searchDB = Server.getSearchDB(); 

	/**
	 * client requests history, format: 12;USER;PASS
	 * server responds with 14;ID,RESULT;ID,RESULT ..
	 */
	@Override
	public void execute(String[] msg, String fromHost, int fromPort) {		
		String responseString = "14;";
		
		// get history from search db
		LinkedList<SearchRequest> results = searchDB.findByUser(msg[1]);
		
		// build the response message
		for (SearchRequest s : results) {
			responseString += s.getId();
			responseString += ",";
			responseString += s.getResult();
			responseString += ";";
		}
		
		// send history to client
		log.info("sending history " + responseString + " to client " + fromHost + ":" + fromPort);
		new MessageDispatcher().dispatchMessage(fromHost, Server.CLIENTPORT, responseString);
	}

}
