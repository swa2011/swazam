package at.ac.tuwien.infosys.shazam.server;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

public class PeerSelectHeuristic {
	private Logger log = Logger.getAnonymousLogger();
	
	/**
	 * get numOfPeers elements from peers selected by a heuristic (simple LIFO here)
	 * @param peers the list of all peers
	 * @param numOfPeers the number to return
	 * @return a list of numOfPeers elements
	 */
	public List<Peer> getPeers(LinkedList<Peer> peers, int numOfPeers) {
		if (numOfPeers <= peers.size()) {
			// return the numOfPeer newest peers
			return peers.subList(peers.size() - numOfPeers , peers.size());
		}
		else {
			log.warning("More peers requested than available, returning all peers available.");
			return peers;
		}
	}	
}
