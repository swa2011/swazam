package at.ac.tuwien.infosys.shazam.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

// entities of the account database.
public class Account implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private static int totalAccounts = 0;
	private int id;

	private String user;
	private int password;
	private String email;
	private int coins;
	
	/**
	 * Constructor
	 * @param user
	 * @param password
	 */
	public Account(String user, String password) {
		this.id = totalAccounts;
		this.user = user;
		this.password = password.hashCode(); // might not be that secure, but proofs our best intentions :-)
		this.coins = Server.INITIALCOINS; // coins for free! yay!
		totalAccounts++;
	}

	/**
	 * check a password
	 * @param pw password given
	 * @return true if given password equals stored password, false otherwise
	 */
	public boolean checkPassword(String pw) {
		return (password == pw.hashCode());
	}
	
	public String getUsername() {
		return user;
	}
	
	public int getId() {
		return id;
	}

	public int getCoins() {
		return coins;
	}
	
	public void setCoins(int c) {
		this.coins = c;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	// needed for de/serialising static counter
	private void writeObject(ObjectOutputStream oos) throws IOException 
	{
		oos.defaultWriteObject();
		oos.writeObject(new Integer(totalAccounts));
	}

	private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException 
	{
		ois.defaultReadObject();
		totalAccounts = (Integer)ois.readObject();
	}
	
	/**
	 * returns a string with information about an account
	 */
	@Override
	public String toString() {
		String s = "";
		
		s += "ID:\t\t" + this.id + "\n";
		s += "USERNAME:\t" + this.user + "\n";
		s += "COINS:\t\t" + this.coins + "\n";

		return s;
	}
}
