package at.ac.tuwien.infosys.shazam.server.test;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import at.ac.tuwien.infosys.shazam.server.Account;
import at.ac.tuwien.infosys.shazam.server.Peer;
import at.ac.tuwien.infosys.shazam.server.PeerDatabase;
import at.ac.tuwien.infosys.shazam.server.Server;

public class PeerDatabaseTest {

	private static PeerDatabase peerDB;
	
	@Before
	public void setUp() throws Exception {
		peerDB = new PeerDatabase();
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * test finding peers in database by ip/port
	 */
	@Test
	public void testAddAndFindPeer() {		
		Account account = new Account("georg", "foo");
		Peer searchPeer = new Peer(account, "127.0.0.1", "1234");
		peerDB.add(new Peer(new Account("foo", "foo"), "127.0.0.2", "1235"));
		peerDB.add(searchPeer);
		peerDB.add(new Peer(new Account("bar", "foo"), "127.0.0.1", "1236"));

		// check number of peers in database
		assertEquals(3, peerDB.size());
		
		// find a peer that is in the database
		Peer foundPeer = peerDB.find("127.0.0.1", "1234");
		assertEquals(searchPeer, foundPeer);

		// don't find a peer that is not
		foundPeer = peerDB.find("127.9.9.9", "9999");
		assertEquals(null, foundPeer);
		
		// find by account
		foundPeer = peerDB.find(account);
		assertEquals(searchPeer, foundPeer);
	}
	
	/**
	 * test retrieving a subset of peers of a given number
	 */
	@Test
	public void testGetPeers() {	
		int numOfPeers = 3;
		List<Peer> returnedPeers;
		List<Peer> expected = new LinkedList<Peer>();
		
		Peer p1 = new Peer(new Account("bar", "foo"), "127.0.0.2", "1235");
		Peer p2 = new Peer(new Account("barfoo", "foo"), "127.0.0.4", "1235");
		Peer p3 = new Peer(new Account("foobar", "pass"), "127.0.0.3", "1235");
		Peer p4 = new Peer(new Account("foo", "foo"), "127.0.0.1", "1236");

		returnedPeers = peerDB.getPeers(numOfPeers);
		assertEquals(returnedPeers.size(), 0);
		
		peerDB.add(p1);
		expected.add(p1);
		
		returnedPeers = peerDB.getPeers(numOfPeers);
		assertEquals(returnedPeers.size(), expected.size());
		assertEquals(expected, returnedPeers);

		expected = new LinkedList<Peer>();
		peerDB.add(p2);
		expected.add(p1);
		expected.add(p2);

		returnedPeers = peerDB.getPeers(numOfPeers);
		assertEquals(returnedPeers.size(), expected.size());
		assertEquals(expected, returnedPeers);

		expected = new LinkedList<Peer>();
		peerDB.add(p3);
		expected.add(p1);
		expected.add(p2);
		expected.add(p3);

		returnedPeers = peerDB.getPeers(numOfPeers);
		assertEquals(returnedPeers.size(), expected.size());
		assertEquals(expected, returnedPeers);
		
		expected = new LinkedList<Peer>();
		peerDB.add(p4);
		expected.add(p2);
		expected.add(p3);
		expected.add(p4);
		
		returnedPeers = peerDB.getPeers(numOfPeers);
		assertEquals(returnedPeers.size(), expected.size());
		assertEquals(expected, returnedPeers);
	}
	
	/**
	 * IP:PORT must be unique 
	 */
	@Test
	public void testPeerDuplicationInDatabase() {
			int numOfPeers = 3;
			List<Peer> returnedPeers;
			List<Peer> expected = new LinkedList<Peer>();
			
			Peer p1 = new Peer(new Account("a", "b"), "127.0.0.2", "1235");
			Peer p2 = new Peer(new Account("a", "b"), "127.0.0.2", "1235");

			peerDB.add(p1);
			peerDB.add(p2);
			
			expected.add(p1);
			
			returnedPeers = peerDB.getPeers(numOfPeers);
			assertEquals(returnedPeers.size(), expected.size());
			assertEquals(expected, returnedPeers);
	}
	
	/**
	 * test max peer db size
	 */
	@Test
	public void testMaxDatabaseSize() {		
		peerDB = new PeerDatabase();
		Peer first, last, expected = null;
		int i;
		
		// fill db with MAXPEERSINDB - 1 peers
		for (i = 0; i < Server.MAXPEERSINDB; i++) {
			peerDB.add(new Peer(new Account("a", "b"), "127.0.0.1", Integer.toString(i)));
			assertEquals(i+1, peerDB.size());
		}

		// fill db to the maximum peer size
		peerDB.add(new Peer(new Account("a", "b"), "127.0.0.1", "9998"));
		assertEquals(Server.MAXPEERSINDB, peerDB.size());
		
		// add one more peer
		expected = new Peer(new Account("a", "b"), "127.0.0.1", "9999");
		peerDB.add(expected);
		assertEquals(Server.MAXPEERSINDB, peerDB.size());

		//127.0.0.1:0 should be gone 127.0.0.1:9999 should be there
		first = peerDB.find("127.0.0.1", "0");
		last = peerDB.find("127.0.0.1", "9999");
		assertEquals(null, first);
		assertEquals(expected, last);
	}
	
	/**
	 * test retrieving a subset of peers of a given number
	 */
	@Test
	public void testSize() {	
		peerDB.add(new Peer(new Account("bar", "foo"), "127.0.0.2", "1235"));
		peerDB.add(new Peer(new Account("barfoo", "foo"), "127.0.0.4", "1235"));
		peerDB.add(new Peer(new Account("foobar", "pass"), "127.0.0.3", "1235"));
		peerDB.add(new Peer(new Account("foo", "foo"), "127.0.0.1", "1236"));
		
		assertEquals(4, peerDB.size());
	}
}