package at.ac.tuwien.infosys.shazam.server;

import java.util.logging.Logger;

import at.ac.tuwien.infosys.shazam.communication.listener.MessageHandler;
import at.ac.tuwien.infosys.shazam.communication.listener.SocketListener;
import at.ac.tuwien.infosys.shazam.server.commands.HandleClientRequestsHistoryCommand;
import at.ac.tuwien.infosys.shazam.server.commands.HandleClientSearchRequestCommand;
import at.ac.tuwien.infosys.shazam.server.commands.HandlePeerRegisterCommand;
import at.ac.tuwien.infosys.shazam.server.commands.HandlePeerRequestsListCommand;
import at.ac.tuwien.infosys.shazam.server.commands.HandlePeerResultCommand;

public class Server {

	private static PeerDatabase peerDB;
	private static AccountDatabase accDB;
	private static SearchDatabase searchDB;

	private static Logger log;	
    
	private static Server instance = new Server();
    
    private static MessageHandler mh;
    
    // server configuration
	public static final int MAXHOPS = 3;			// hops until search stops, peers must decrease this value
	public static final int NUMOFSEEDPEERS = 3;		// how many peers shall get the searchrequest from the server
	public static final int PEERLISTSIZE = 3;		// how many peers shall a new peer get on first contact
	public static final int MAXPEERSINDB = 20;		// maximum peers to hold in the database
	public static final int CLIENTPORT = 7777;		// FIXME or not. hardcoded port for client communication
	public static final int INITIALCOINS = 100;		// number of coins every new account gets
	public static String SERVER_IP = "127.0.0.1";	// the ip this server is running on
	public static int SERVER_PORT = 12345;			// the port the server listens on for incoming messages
	
	/**
	 * main
	 * @param args server_port or server_ip and server_port, defaults to 127.0.0.1 and 12345  
	 */
	public static void main(String[] args) {
		log = Logger.getAnonymousLogger();		
		log.info("Starting server...");

		// handle arguments if any
		if (args.length == 1) {
			SERVER_PORT = Integer.valueOf(args[0]);
		}
		else if (args.length == 2) {
			SERVER_IP = args[0];
			SERVER_PORT = Integer.valueOf(args[1]);
		}
		else if (args.length == 0) {
			log.info("no arguments given, using default values " + SERVER_IP + " " + SERVER_PORT + "\n" +
			"to specify your own port start with \"java at....server.Server PORT\"");
		}
		
		// load or initialize databases
		loadDatabases();

		mh = new MessageHandler();

		// Callbacks
		// peer registers itself with server, format: 1;user;pass;192.168.1.1:5432
		mh.registerMessageHandler(1, new HandlePeerRegisterCommand());
		// peer requests a list of peers, format (subject to change?): 3:192.168.1.2:4323
		mh.registerMessageHandler(3, new HandlePeerRequestsListCommand());
		// peer sends result for a search, format: 6;user;pass;HASH;RESULT
		mh.registerMessageHandler(6, new HandlePeerResultCommand());
		// client sends a search request of format: 10;USER;PASS;HASH
		mh.registerMessageHandler(10, new HandleClientSearchRequestCommand());
		// client requests history, format: 12;USER;PASS
		mh.registerMessageHandler(12, new HandleClientRequestsHistoryCommand());

		// initialize socket listener
		SocketListener listener = new SocketListener(SERVER_IP, SERVER_PORT, mh);

		try {
			new Thread(listener).start();
		} catch (Exception e) {
			log.warning("Could not create SocketListener on port " + SERVER_PORT + "\n" + e);
			e.printStackTrace();
		}
	}

    public static Server getInstance() {
        return instance;
    }

	public static PeerDatabase getPeerDB() {
		return peerDB;
	}

	public static AccountDatabase getAccDB() {
		return accDB;
	}

	public static SearchDatabase getSearchDB() {
		return searchDB;
	}
	
	/**
	 * loads (or creates, if loading fails) databases from files
	 */
	public static void loadDatabases() {
		DatabaseSerializer dbs = new DatabaseSerializer();

		peerDB =  (PeerDatabase) dbs.deserialize("peer.db");
		if (peerDB == null) {
			log.info("couldn't load peer database, creating a new one");
			peerDB = new PeerDatabase();			
		}
		else {
			peerDB.setLogger();
			peerDB.setHeuristic();
			peerDB.setSerializer();
			log.info("loaded peer database with " + peerDB.size() + " entries");
			log.info(peerDB.toString());
		}
		accDB = (AccountDatabase) dbs.deserialize("account.db");
		if (accDB == null) {
			log.info("couldn't load account database, creating a new one");
			accDB = new AccountDatabase();
		}
		else {
			accDB.setLogger();
			accDB.setSerializer();
			log.info("loaded account database with " + accDB.size() + " entries");
			log.info(accDB.toString());
		}
		searchDB = (SearchDatabase) dbs.deserialize("search.db");
		if (searchDB == null) {
			log.info("couldn't load search database, creating a new one");
			searchDB = new SearchDatabase();
		}
		else {
			searchDB.setLogger();
			searchDB.setSerializer();
			log.info("loaded search database with " + searchDB.size() + " entries");
			log.info(searchDB.toString());
		}
	}
}