package at.ac.tuwien.infosys.shazam.server;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

// stores peer information and provides access to them
public class PeerDatabase implements ServerDatabase, Serializable {

	private static final long serialVersionUID = 1L;

	private transient Logger log;
	private transient DatabaseSerializer dbs;
	private transient PeerSelectHeuristic psh;
	private LinkedList<Peer> peers = new LinkedList<Peer>();
	
	
	/**
	 * Constructor
	 */
	public PeerDatabase() {
		setLogger();
		setHeuristic();
		setSerializer();
	}

	public void setLogger() {
		log = Logger.getAnonymousLogger();
		log.info("PeerDB up and running");
	}
	
	public void setSerializer() {
		dbs = new DatabaseSerializer();
	}
	
	public void setHeuristic() {
		this.psh = new PeerSelectHeuristic();
	}
	
	/**
	 * return the number of peers in the database
	 */
	public int size() {
		return peers.size();
	}
	
	/**
	 * get a number of peers, selected by a fixed heuristic (LIFO for the moment)
	 * @param numOfPeers the number of peers to return
	 * @return a list of peers
	 */
	public List<Peer> getPeers(int numOfPeers) {
		return psh.getPeers(peers, numOfPeers);
	}
	
	/**
	 * add a peer to the database
	 * @param p the peer to add
	 */
	public synchronized void add(Peer p) {
		if (this.find(p.getIp(), p.getPort()) != null) {
			log.info("peer " + p.getIp() + ":" + p.getPort() + " already in database, aborting...");
			return;
		}
		
		if(peers.size() >= Server.MAXPEERSINDB) {
			log.info("Maximum number of peers to store reached, removing the oldest");
			peers.removeFirst();
		}
		
		peers.add(p);
		log.info("added peer: " + p + " " + p.getId());
		
		save();
	}
	
	/**
	 * find a peer by IP:PORT
	 * @param ip the IP of the peer to find
	 * @param port the port of the peer to find
	 * @return a peer or null, if the peer is not in the database
	 */
	public Peer find(String ip, String port) {
		for (Peer p : peers) {
			if (p.getIp().equals(ip) && p.getPort().equals(port)) {
				return p;				
			}
		}
		return null;
	}

	/**
	 * find a peer by its account data
	 * @param a an account
	 * @return a peer if found, null else
	 */
	public Peer find(Account a) {
		for (Peer p : peers) {
			if (p.getAccount().getUsername().equals(a.getUsername())) {
				return p;	
			}
		}
		return null;
	}
	
	/**
	 * serialize db to file
	 */
	public void save() {
		dbs.serialize(this, "peer.db");
	}
	
	public LinkedList<Peer> getPeerList() {
		return peers;
	}
	
	/**
	 * returns a string with information about all accounts
	 */
	@Override
	public String toString() {
		String s = "";
		
		for (Peer p : peers)
			s += p.toString();
			
		return s;
	}
}

