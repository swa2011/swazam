package at.ac.tuwien.infosys.shazam.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import at.ac.tuwien.infosys.shazam.communication.dispatcher.MessageDispatcher;

// entities of the peer database. provides interface for communication
public class Peer implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private static int totalPeers = 0;
	private int id;

	private Account account;

	private String ip;
	private String port;
	private int songsFound = 0;

	public Peer(Account account, String ip, String port) {
		this.id = totalPeers;
		this.ip = ip;
		this.port = port;
		this.account = account;
		
		totalPeers++;
	}

	public String getUsername() {
		return account.getUsername();
	}
	
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public int getId() {
		return id;
	}

	public int getSongsFound() {
		return songsFound;
	}

	public void setSongsFound(int songsFound) {
		this.songsFound = songsFound;
	}

	/**
	 * send a message to the peer
	 * @param msg the message to send
	 */
	public synchronized void sendMessage(String msg) {
		new MessageDispatcher().dispatchMessage(this.ip, Integer.valueOf(this.port), msg);
	}

	public Account getAccount() {
		return account;
	}

	// needed for de/serialising static counter
	private void writeObject(ObjectOutputStream oos) throws IOException 
	{
		oos.defaultWriteObject();
		oos.writeObject(new Integer(totalPeers));
	}

	private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException 
	{
		ois.defaultReadObject();
		totalPeers = (Integer)ois.readObject();
	}
	
	/**
	 * returns a string with information about an account
	 */
	@Override
	public String toString()  {
		String s = "PEER:\n";
		s += this.account.toString();
		s += "SONGS FOUND\t:" + this.songsFound + "\n";
		return s;
	}
}
