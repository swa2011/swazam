package at.ac.tuwien.infosys.shazam.server;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.logging.Logger;

// stores search information and provides access to them
public class SearchDatabase implements ServerDatabase, Serializable {

	private static final long serialVersionUID = 1L;
	
	private transient Logger log;
	private transient DatabaseSerializer dbs;
	private LinkedList<SearchRequest> searchRequests = new LinkedList<SearchRequest>();

	/**
	 * Constructor
	 */
	public SearchDatabase() {
		setLogger();
		setSerializer();
	}
	
	public void setLogger() {
		log = Logger.getAnonymousLogger();
		log.info("PeerDB up and running");
	}
	
	public void setSerializer() {
		dbs = new DatabaseSerializer();
	}
	
	/**
	 * returns the size of the db
	 */
	public int size() {
		return searchRequests.size();
	}

	/**
	 * add a search request to the db
	 * @param s a searchrequest
	 */
	public synchronized void add(SearchRequest s) {
		searchRequests.add(s);
		log.info("added searchrequest");
		
		save();
	}
	
	/**
	 * find searchrequests by id
	 * @param id the id to look for
	 * @return a searchrequest or null if id is not found
	 */
	public SearchRequest find(int id) {
		for (SearchRequest s : searchRequests) {
			if (s.getId() == id) {
				return s;				
			}
		}
		return null;
	}
	
	/**
	 * returns all search requests for a given username
	 * @param user the username to look for
	 * @return a linked list of all searchrequests by the given user
	 */
	public LinkedList<SearchRequest> findByUser(String user) {
		LinkedList<SearchRequest> results = new LinkedList<SearchRequest>();
		
		for (SearchRequest s : searchRequests) {
			if (s.getFromUser().equals(user)) {
				results.add(s);
			}
		}
		return results;
	}

	/**
	 * serialize db to file
	 */
	public void save() {
		dbs.serialize(this, "search.db");
	}
	
	/**
	 * returns information about all search requests
	 */
	@Override
	public String toString() {
		String s = "";
		
		s += "\nID\t\tFROM\t\tFOUND\t\tRESULT\n";
		
		for (SearchRequest r: searchRequests)
			s += r.toString();

		return s;
	}
}
