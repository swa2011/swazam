package at.ac.tuwien.infosys.shazam.client.commands;


import java.util.HashMap;
import java.util.Observable;

import at.ac.tuwien.infosys.shazam.client.NetComunicator;
import at.ac.tuwien.infosys.shazam.client.RequestHandler;
import at.ac.tuwien.infosys.shazam.communication.listener.Command;
import at.ac.tuwien.infosys.shazam.shared.Song;


/**
 * Handles history responses from server
 * If the history not includes all requested songs a new request will be sent
 * 
 * @author david
 *
 */
public class HandleServerHistoryReplyCommand extends Observable implements Command {
	NetComunicator netComunicator;

	String host;
	int port;

	public HandleServerHistoryReplyCommand(String host, int port) {
		super();
		this.host = host;
		this.port = port;
	}
	public HandleServerHistoryReplyCommand(){
		
	}

	@Override
	public void execute(String[] response, String fromHost, int fromPort) {
		netComunicator = NetComunicator.getInstance();
		if (response.length >= 2) {

			int responseCounter = 0;

			// For every song response from server restore fingerprint
			// and get info
			for (int i = 1; i < response.length; i++) {
				String[] responsSplitted = response[i].split(",");

				HashMap<Integer, Song> songRequest = netComunicator.getSongRequest();
				synchronized (songRequest) {
					if (songRequest.containsKey(Integer.parseInt(responsSplitted[0]))) {
						
						//If data is not null or "null"
						if(!(responsSplitted[responsSplitted.length - 1] == null || responsSplitted[responsSplitted.length - 1].equals("null"))){
							responseCounter++;
							songRequest
									.get(Integer.parseInt(responsSplitted[0]))
									.setArtist(
											responsSplitted[responsSplitted.length - 1]);
							System.out.println("Artist: "
									+ songRequest.get(Integer.parseInt(responsSplitted[0])).getArtist());
						}
					}
				}

			}

			//If not all requests are answered send a new request
			if (responseCounter < netComunicator.getSongRequest().size()) {
				netComunicator.requestHistory(new RequestHandler(host,port));
			}else if (responseCounter > 0){
				//reset the delay to 0,5 seconds so next history request won't take ages
				netComunicator.setRequestDelay(500);
				
				setChanged();
				notifyObservers(response);
				clearChanged();
			}
		} else {
			netComunicator.requestHistory(new RequestHandler(host,port));
		}
	}

}
