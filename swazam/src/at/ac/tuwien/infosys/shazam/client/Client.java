/**
 * 
 */
package at.ac.tuwien.infosys.shazam.client;

import java.io.File;
import java.util.HashSet;
import java.util.Observable;

import at.ac.tuwien.infosys.shazam.client.commands.HandleServerHistoryReplyCommand;
import at.ac.tuwien.infosys.shazam.shared.Song;
import at.ac.tuwien.infosys.swa.audio.Fingerprint;

/**
 * Command line interface for querying swazam 
 * @author david
 * 
 */
public class Client extends AbstractClient {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length == 2) {
			for (int i = 0; i < args.length; i++) {
				System.out.println("Argument" + i + ": " + args[i]);
			}
			new Client(args[1]);
		} else {
			System.out
					.println("Wrong number of arguments!\n Usage: java Client query path/to/song.mp3");
		}
	}

	public Client(String path) {
		super();
		Song s = new Song();
		s.setLocalPath(path);
		getSongs().add(s);
		queryFromMp3File(new File(path));
	}

	@Override
	public void update(Observable o, Object arg) {
		//Fingerprint creator is done with creating a fingerprint for a file
		if(o instanceof at.ac.tuwien.infosys.shazam.client.FingerprintCreator){
			fingerprintUpdate((FingerprintCreator) o, (File) arg);
		//sets the id for the requested song
		}else if(o instanceof NetComunicator && arg instanceof Integer){
			setSongId((Integer) arg);
		//this will be triggered if there is new info for song(s) 
		} else if( o instanceof HandleServerHistoryReplyCommand){
			synchronized (songs) {				
				for(Song s : songs){
					if(getNetComunicator().getSongRequest().size() > 0){
						Song t = getNetComunicator().getSongRequest().get(s.getId());
						if(t !=null){
							System.out.println("File " + s.getLocalPath() + "is: " + t.getArtist());
							System.exit(0);
						}
					}
				}
			}
		}
	}
}
