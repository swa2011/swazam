/**
 * 
 */
package at.ac.tuwien.infosys.shazam.client;

import java.util.*;
import java.util.concurrent.Semaphore;

import at.ac.tuwien.infosys.shazam.client.commands.HandleServerACKsCommand;
import at.ac.tuwien.infosys.shazam.client.commands.HandleServerHistoryReplyCommand;
import at.ac.tuwien.infosys.shazam.communication.listener.Command;
import at.ac.tuwien.infosys.shazam.communication.listener.MessageHandler;
import at.ac.tuwien.infosys.shazam.communication.listener.SocketListener;
import at.ac.tuwien.infosys.shazam.shared.Song;
import at.ac.tuwien.infosys.swa.audio.Fingerprint;
import at.ac.tuwien.infosys.swa.audio.SubFingerprint;

/**
 * Singleton class that handles communication with the server.
 * @author david
 * @see {@link RequestHandler}, {@link MessageHandler}, {@link Command} 
 */
public class NetComunicator extends Observable {

	private String host = "127.0.0.1";
	private int port = 12345;
	
	//Map of requested songs
	private HashMap<Integer, Song> songRequest;
	//private singleton NetComunicator
	private static NetComunicator nc;
	//RequestHandler for handling history requests
	private RequestHandler historyRequester;
	//Initial delay of a history request
	private long requestDelay = 500;
	//Synchronization objects
	private Object historySync, sendLookup;
	private Semaphore acked;
	//IP and port where the client listens for incoming messages
	public static final String LISTEN_IP = "127.0.0.1";
	public static int LISTEN_PORT = 7777;
	//The clallbacks for handling incoming messages from server
	private HandleServerHistoryReplyCommand handleServerHistoryReplyCommand;
	private HandleServerACKsCommand handleServerACKsCommand;
	
	/**
	 * 
	 * @return instance of NetComunicator
	 */
	public static synchronized NetComunicator getInstance() {
		if (null == nc) {
			nc = new NetComunicator();
		}
		return nc;
	}
	/**
	 * 
	 * @param host server host
	 * @param port server port
	 * @return instance of NetComunicator
	 */
	public static synchronized NetComunicator getInstance(String host, int port) {
		if (null == nc) {
			nc = new NetComunicator(host,port);
		}else{
			nc.setHost(host);
			nc.setPort(port);
		}
		return nc;
	}

	private NetComunicator() {
		this("127.0.0.1", 12345);
	}
	
	/**
	 * Private constructor called by the {@link NetComunicator#getInstance(String, int)} if no instance exists
	 * @param host server host
	 * @param port server port
	 */
	private NetComunicator(String host, int port) {
		super();
		this.host = host;
		this.port = port;
		//Create synchronization objects
		historySync = new Object();
		sendLookup = new Object();
		//Semaphore for determine that id is looked up
		acked = new Semaphore(0);
		//
		songRequest = new HashMap<Integer, Song>();
		
		handleServerHistoryReplyCommand = new HandleServerHistoryReplyCommand(host,port);
		handleServerACKsCommand = new HandleServerACKsCommand();
		MessageHandler mh = new MessageHandler();


		// Callbacks
		mh.registerMessageHandler(11, handleServerACKsCommand);
		mh.registerMessageHandler(14, handleServerHistoryReplyCommand);


		// initialize socket listener
		SocketListener listener = new SocketListener(LISTEN_IP, LISTEN_PORT, mh);

		try {
			new Thread(listener).start();
		} catch (Exception e) {
			// TODO log.warning("Could not create SocketListener on port " + SERVER_PORT + "\n" + e);
			e.printStackTrace();
		}
	}

	/**
	 * Send a lookup request to the server defined by {@link NetComunicator#host} and {@link NetComunicator#port}
	 * @param song The Song to retrieve information about
	 * @param username
	 * @param password
	 */
	public void sendLookup(Song song, String username,
			String password) {
		//synchronizing sendLookup
		synchronized (sendLookup) {
			RequestHandler rh = new RequestHandler(host, port);
			Fingerprint splittedPrint = song.getFingerprint().split()[0];
	
			String request = "" + 10 + ";" + username + ";" + password + ";"
					+ splittedPrint.getShiftDuration();
			SubFingerprint[] sf = splittedPrint.getSubFingerprints();
			

			for (SubFingerprint s : sf) {
				request += ";" + s.getValue();
			}
			requestDelay = 500;
			historyRequester = null;
			System.out.println("request:" + request);
			handleServerACKsCommand.setWaitingForAck(song);
			
			rh.sendRequest(request);
			try {
				acked.acquire();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("semaphore released");
		}
	}
	
	/**
	 * Determine if a {@link RequestHandler} are allowed to send a history request. Only one request is allowed at the time 
	 * 
	 * @param requestHandler The {@link RequestHandler} that would like to send a history request
	 */
	public void requestHistory(RequestHandler requestHandler) {
		synchronized (historySync) {
			if (historyRequester == null) {
				historyRequester = requestHandler;
			}
		}
			requestDelay = requestDelay * 2;
			if (requestDelay > 3600000)
				requestDelay = 3600000;
			System.out.println("Sending new history request to server");
					requestHandler.sendDelayedRequest("12;user;qwerty", requestDelay);
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getPort() {
		return port;
	}

	public HashMap<Integer, Song> getSongRequest() {
		return songRequest;
	}

	public void setSongRequest(HashMap<Integer, Song> songRequest) {
		this.songRequest = songRequest;
	}

	public long getRequestDelay() {
		return requestDelay;
	}

	public void setRequestDelay(long requestDelay) {
		this.requestDelay = requestDelay;
	}
	
	public void putSongRequest(int id, Song song){
		songRequest.put(id, song);
		setChanged();
		notifyObservers(id);
		clearChanged();
	}

	public RequestHandler getHistoryRequester() {
		return historyRequester;
	}

	public void setHistoryRequester(RequestHandler historyRequester) {
		this.historyRequester = historyRequester;
	}

	public HandleServerHistoryReplyCommand getHandleServerHistoryReplyCommand() {
		return handleServerHistoryReplyCommand;
	}

	public void setHandleServerHistoryReplyCommand(
			HandleServerHistoryReplyCommand handleServerHistoryReplyCommand) {
		this.handleServerHistoryReplyCommand = handleServerHistoryReplyCommand;
	}

	public Semaphore getAcked() {
		return acked;
	}

	public void setAcked(Semaphore acked) {
		this.acked = acked;
	}
	
}
