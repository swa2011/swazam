package at.ac.tuwien.infosys.shazam.client;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Observable;

import at.ac.tuwien.infosys.shazam.client.commands.HandleServerHistoryReplyCommand;
import at.ac.tuwien.infosys.shazam.shared.*;
import at.ac.tuwien.infosys.swa.audio.Fingerprint;
/**
 * Model class for the mvc pattern in the graphical client
 * 
 * @author david
 *
 */
public class ClientModel extends AbstractClient{
	
	
	@Override
	public void update(Observable arg0, Object arg1) {
		//adds fingerprint to song
		if(arg0 instanceof at.ac.tuwien.infosys.shazam.client.FingerprintCreator){
			fingerprintUpdate((FingerprintCreator) arg0, (File) arg1);
		//adds server assigned id to song
		}else if(arg0 instanceof NetComunicator && arg1 instanceof Integer){
			setSongId((Integer) arg1);
		//Handles the info for a identified song
		}else if(arg0 instanceof HandleServerHistoryReplyCommand){
			synchronized (songs) {
				HashSet<Integer> requestsToRemove = new HashSet<Integer>();
				
				for(Song s : songs){
					if(getNetComunicator().getSongRequest().size() > 0){
						Song t = getNetComunicator().getSongRequest().get(s.getId());
						if(t !=null){
							if(! (t.getAlbum().equals("") && t.getArtist().equals("") && t.getTitle().equals(""))){
								s.setAlbum(t.getAlbum());
								s.setArtist(t.getArtist());
								s.setTitle(t.getTitle());
								requestsToRemove.add(s.getId());
							}
						}
					}
				}
				for(Integer f : requestsToRemove){
					getNetComunicator().getSongRequest().remove(f);
				}
			}
			setChanged();
			notifyObservers();
			clearChanged();
		}
	}
}
