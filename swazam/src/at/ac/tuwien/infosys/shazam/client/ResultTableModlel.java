package at.ac.tuwien.infosys.shazam.client;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import at.ac.tuwien.infosys.shazam.shared.Song;
/**
 * Graphical model for viewing information about a song
 * @author david
 *
 */
public class ResultTableModlel extends AbstractTableModel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7163893012916786809L;
	public final String[] HeaderNames = {"Path","Song Info"};
	private ArrayList<Song> songs;
	
	@Override
	public String getColumnName(int column) {
		return HeaderNames[column];
	};
	
	public ResultTableModlel(){
		super();
		songs = new ArrayList<Song>();
	}
	
	@Override
	public int getColumnCount() {
		return HeaderNames.length;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return songs.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
			case 0:
				return songs.get(rowIndex).getLocalPath();
			case 1:
//				return songs.get(rowIndex).getTitle();
//			case 2:
				return songs.get(rowIndex).getArtist();
//			case 3:
//				return songs.get(rowIndex).getAlbum();
		}
		return "";
	}

	public ArrayList<Song> getSongs() {
		return songs;
	}

	public void setSongs(ArrayList<Song> songs) {
		this.songs = songs;
	}
}
