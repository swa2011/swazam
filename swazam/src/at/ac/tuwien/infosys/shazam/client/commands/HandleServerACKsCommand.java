package at.ac.tuwien.infosys.shazam.client.commands;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import at.ac.tuwien.infosys.shazam.client.NetComunicator;
import at.ac.tuwien.infosys.shazam.client.RequestHandler;
import at.ac.tuwien.infosys.shazam.communication.listener.Command;
import at.ac.tuwien.infosys.shazam.peer.Peer;
import at.ac.tuwien.infosys.shazam.shared.Song;
import at.ac.tuwien.infosys.swa.audio.Fingerprint;
import at.ac.tuwien.infosys.swa.audio.SubFingerprint;

/**
 * Handle acks from server
 * @author david
 *
 */
public class HandleServerACKsCommand implements Command {
	private Song waitingForAck;
	
	public HandleServerACKsCommand(){
	}

	
	@Override
	public void execute(String[] msg, String fromHost, int fromPort) {

		//System.out.println("request came back!");
		RequestHandler rh = new RequestHandler(NetComunicator.getInstance().getHost(), NetComunicator.getInstance().getPort());
		rh.sendRequest("12;user;qwerty");
		if(msg.length>1){
			waitingForAck.setId(Integer.parseInt(msg[1]));
			NetComunicator.getInstance().putSongRequest(Integer.parseInt(msg[1]), waitingForAck);
			Song s = NetComunicator.getInstance().getSongRequest().get(waitingForAck);
			System.out.println("releasing");
			NetComunicator.getInstance().getAcked().release();
		}
	}
	public Song getWaitingForAck() {
		return waitingForAck;
	}
	public void setWaitingForAck(Song waitingForAck) {
		this.waitingForAck = waitingForAck;
	}
	
}
