package at.ac.tuwien.infosys.shazam.client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import at.ac.tuwien.infosys.shazam.client.ResultTableModlel;
import at.ac.tuwien.infosys.shazam.shared.Song;
import at.ac.tuwien.infosys.swa.audio.Fingerprint;

public class ClientController {
	
	private AbstractClient model;
	private ArrayList<GrapicalClient> views;
	private File choosenFile;
	
	public ClientController(){
		model = new ClientModel();
		views = new ArrayList<GrapicalClient>();
	}
	
	public AbstractClient registerView(final GrapicalClient view){
		views.add(view);
		model.addObserver(view);
		
		view.getFileChooser().setFileFilter(new FileFilter() {
			
			@Override
			public String getDescription() {
				return "Music files - *.mp3";
			}
			
			@Override
			public boolean accept(File arg0) {
				String name = arg0.getName().substring(arg0.getName().length()-3, arg0.getName().length());
				return (name.equals("mp3") || arg0.isDirectory());
			}
		});
		
		//Listners for the different interactions in the gui
		
		view.getCloseButton().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
				
			}
		});
		
		view.getChooseFileButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				view.getSendRequestButton().setEnabled(false);
				view.getFileLabel().setText("no file selected");
				int returnVal =  view.getFileChooser().showOpenDialog(view);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					choosenFile = view.getFileChooser().getSelectedFile();
					view.getFileLabel().setText("File: " + choosenFile.getName());
					view.getSendRequestButton().setEnabled(true);
					view.pack();
				}
			}
		});
		
		view.getSendRequestButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (choosenFile != null){
					Song s = new Song();
					s.setLocalPath(choosenFile.getPath());
					s.setTitle("Not found yet");
					model.getSongs().add(s);
					model.queryFromMp3File(choosenFile);
					view.getFileLabel().setText("no file selected");
					view.getSendRequestButton().setEnabled(false);
				}
				
			}
		});
		
		view.getClearButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				view.setResultmodel(new ResultTableModlel());
				view.getResult().setModel(view.getResultmodel());
				model.getNetComunicator().setSongRequest(new HashMap<Integer, Song>());
				model.setSongs(new ArrayList<Song>());
				view.repaint();
			}
		});
		
		return model;
	}

}
