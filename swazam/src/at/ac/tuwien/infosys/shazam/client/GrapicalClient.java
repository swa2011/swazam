/**
 * 
 */
package at.ac.tuwien.infosys.shazam.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.border.EtchedBorder;
import javax.swing.table.TableModel;

/**
 * Graphical view for the client
 * view component in the mvc pattern
 * @author david
 *
 */
public class GrapicalClient extends JFrame implements Observer {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3916041773790402242L;
	private JPanel northPanel, southPanel, centerPanel;
	private JButton clearButton, closeButton, chooseFileButton, sendRequestButton;
	private JTable result;
	private ResultTableModlel resultmodel;
	private JFileChooser fileChooser;
	private JLabel fileLabel;
	
	private AbstractClient model;
	private ClientController clientController;
	
	
	public GrapicalClient(){
		super();
		
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		northPanel = new JPanel();
		centerPanel = new JPanel();
		southPanel = new JPanel();
		closeButton = new JButton("close swazam");
		clearButton = new JButton("clear history");
		chooseFileButton = new JButton("Choose file");
		sendRequestButton  = new JButton("Find song info");
		resultmodel = new ResultTableModlel();
		result = new JTable(resultmodel);
		fileChooser = new JFileChooser();
		fileLabel = new JLabel("no file selected");
		
		clientController = new ClientController();
		model = clientController.registerView(this);
		initGUI();
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new GrapicalClient();

	}
	
	/**
	 * Initialize the graphical user interface 
	 */
	private void initGUI(){
		setLayout(new BorderLayout());
		sendRequestButton.setEnabled(false);
		northPanel.setLayout(new BoxLayout(northPanel, BoxLayout.X_AXIS));
		northPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		
		northPanel.add(chooseFileButton);
		northPanel.add(Box.createHorizontalStrut(5));
		northPanel.add(fileLabel);
		northPanel.add(Box.createHorizontalStrut(5));
		northPanel.add(Box.createHorizontalGlue());
		northPanel.add(sendRequestButton);
		
		
		centerPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
		
		result.setBackground(Color.white);
		
		JScrollPane scrollPane = new JScrollPane(result);
		scrollPane.getViewport().setBackground(Color.white);
		scrollPane.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		centerPanel.add(Box.createHorizontalGlue());
		centerPanel.add(scrollPane);
		centerPanel.add(Box.createHorizontalGlue());
		
		southPanel.setLayout(new BoxLayout(southPanel, BoxLayout.X_AXIS));
		southPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		southPanel.add(Box.createHorizontalGlue());
		southPanel.add(clearButton);
		southPanel.add(Box.createHorizontalStrut(5));
		southPanel.add(closeButton);
		
		add(northPanel,BorderLayout.NORTH);
		add(centerPanel, BorderLayout.CENTER);
		add(southPanel, BorderLayout.SOUTH);
		
		result.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		
		pack();
		setVisible(true);

	}
	/**
	 * update the GUI and repaints the table
	 */
	@Override
	public void update(Observable o, Object arg) {
		SwingUtilities.invokeLater(new Runnable() {
		    @Override
			public void run() {
				repaintTable();
		    }
		});
	}
	
	private void repaintTable(){
		resultmodel.setSongs(model.getSongs());
		result.setModel(resultmodel);
		result.updateUI();
		result.repaint();
	}
	
	
	
	
	
	
	// *********** Setters and Getters ****************
	
	public JButton getClearButton() {
		return clearButton;
	}

	public void setClearButton(JButton clearButton) {
		this.clearButton = clearButton;
	}

	public JButton getCloseButton() {
		return closeButton;
	}

	public void setCloseButton(JButton closeButton) {
		this.closeButton = closeButton;
	}

	public JButton getChooseFileButton() {
		return chooseFileButton;
	}

	public void setChooseFileButton(JButton chooseFileButton) {
		this.chooseFileButton = chooseFileButton;
	}

	public JButton getSendRequestButton() {
		return sendRequestButton;
	}

	public void setSendRequestButton(JButton sendRequestButton) {
		this.sendRequestButton = sendRequestButton;
	}

	public JTable getResult() {
		return result;
	}

	public void setResult(JTable result) {
		this.result = result;
	}

	public TableModel getResultmodel() {
		return resultmodel;
	}

	public void setResultmodel(ResultTableModlel resultmodel) {
		this.resultmodel = resultmodel;
	}

	public JFileChooser getFileChooser() {
		return fileChooser;
	}

	public void setFileChooser(JFileChooser fileChooser) {
		this.fileChooser = fileChooser;
	}

	public JLabel getFileLabel() {
		return fileLabel;
	}

	public void setFileLabel(JLabel fileLabel) {
		this.fileLabel = fileLabel;
	}
}
