/**
 * 
 */
package at.ac.tuwien.infosys.shazam.client;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Class that handles a server request
 * @author david
 * @see {@link NetComunicator}
 */
public class RequestHandler {
	private final String host;
	private final int port;
	
	public RequestHandler(String host,int port){
		this.host = host;
		this.port = port;
	}
	/**
	 * sends a Delayed request
	 * @param request request to be sent
	 * @param time delay time
	 */
	public void sendDelayedRequest(String request, long time){
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		sendRequest(request);
		//return 
	}
	/**
	 * Sends a request to the server specified {@link RequestHandler#host} and {@link RequestHandler#port}
	 * @param request String request to be sent to server
	 */
	public void sendRequest(String request) {
		try {
			Socket clientSocket = new Socket(host, port);
			
			PrintWriter out = new PrintWriter(clientSocket.getOutputStream(),
					true);
			out.println(request);

			out.close();
			clientSocket.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		//return fromServer;
	}
}
