/**
 * 
 */
package at.ac.tuwien.infosys.shazam.client;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import at.ac.tuwien.infosys.swa.audio.Fingerprint;
import at.ac.tuwien.infosys.swa.audio.FingerprintSystem;

/**
 * Class for creating a fingerprint from a file
 * Also has threading functionallity. When the Class is started every file in {@link FingerprintCreator#inputFiles} will become a fingerprint 
 * @author david
 *
 */
public class FingerprintCreator extends Observable implements Runnable{
	
	private ArrayList<File> inputFiles;
	HashMap<File, Fingerprint> fingerprintMapper;
	
	public FingerprintCreator(){
		inputFiles = new ArrayList<File>();
		fingerprintMapper = new HashMap<File, Fingerprint>();
	}
	
	/**
	 * Generates a fingerprint from a file
	 * @param inputFile file to generate fingerprint for
	 * @return {@link Fingerprint}
	 * @throws FileNotFoundException
	 */
	public Fingerprint creatFingerprintFromFile(File inputFile) throws FileNotFoundException{
		Fingerprint f = null;
		try {
			AudioInputStream in = AudioSystem.getAudioInputStream(inputFile);
			
			//fingerprintSystem = new FingerprintSystem(in.getFormat().getSampleRate());
			System.out.println("Start generating fingerprint");
			f =  FingerprintSystem.fingerprint(in);
			System.out.println("Fingerprint generated");
			Fingerprint subF = f.split()[0];
//			System.out.println("Fingerprint match: " + f.match(subF) + " " + subF.match(f));
//			System.out.println("Fingerprint: subF: " + subF.hashCode() + " f:" + f.hashCode());
//			System.out.println("Done!");
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return f;
	}
	/**
	 * generates a fingerprint for every file in {@link FingerprintCreator#inputFiles}
	 */
	@Override
	public void run() {
		for(File f : inputFiles){
			 try {
				 fingerprintMapper.put(f,creatFingerprintFromFile(f));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			setChanged();
			notifyObservers(f);
			clearChanged();
		}
	}

//	public ArrayList<File> getInputFiles() {
//		return inputFiles;
//	}
	public void addFileForFingerprintCreation(File file){
		inputFiles.add(file);
	}
	
	public void setInputFiles(ArrayList<File> inputFiles) {
		this.inputFiles = inputFiles;
	}
	
	public Fingerprint getFingerprint(File f){
		return fingerprintMapper.get(f);
	}
}
