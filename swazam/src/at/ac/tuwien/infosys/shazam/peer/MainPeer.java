package at.ac.tuwien.infosys.shazam.peer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class MainPeer {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// File f = null;
		// AudioInputStream ina = null;
		// f = new File("C:/Users/Salah/Desktop/215923.mp3");
		// String test = "C:/Users/Salah/Desktop/215923.mp3";
		// String fileName = test.split("/")[test.split("/").length - 1];
		// System.out.println(fileName);
		// try {
		// ina = AudioSystem.getAudioInputStream(f);
		// System.out.println(ina.hashCode());
		// } catch (UnsupportedAudioFileException e1) {
		// // TODO Auto-generated catch block
		// e1.printStackTrace();
		// } catch (IOException e1) {
		// // TODO Auto-generated catch block
		// e1.printStackTrace();
		// }
		//
		// try {
		// System.out.println(FingerprintSystem.fingerprint(ina));
		// } catch (IOException e1) {
		// // TODO Auto-generated catch block
		// e1.printStackTrace();
		// }

		Map<String, Peer> peers = new HashMap<String, Peer>();
		Peer p = null;
		System.out.println("Available instructions :");
		System.out.println("1) To start a peer : start peerID");
		System.out.println("2) Add a sample to a peer : add peerID Test10.mp3");

		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String s;
		try {

			while ((s = in.readLine()) != null && s.length() != 0) {
				String[] instruction;
				instruction = s.split(" ");

				if (instruction[0].equals("start")) {
					p = new Peer();
					p.setUserName(instruction[1]);
					p.setPassword("password");
					p.setIp("localhost");
					p.setPort(Integer.valueOf(instruction[2]));
					peers.put(instruction[1], p);
					System.out.println("A new peer with the name " + instruction[1] + " has been generated");
					p.run();
				} else if (instruction[0].equals("add")) {
					if (peers.keySet().contains(instruction[1]))
						System.out.println("peer is known");
					System.out.println("Path of the song to add " + instruction[2]);
					peers.get(instruction[1]).getSampleDB().add(instruction[2]);
					System.out.println("A new song with the name "
							+ instruction[2].split("/")[instruction[2].split("/").length - 1]
							+ " has been added to the list of known songs by the peer with username " + instruction[1]);
				} else if (instruction[0].equals("info"))
					for (String peerName : peers.keySet())
						System.out.println("The number of known songs for the peer  " + peerName + " is "
								+ peers.get(peerName).getSampleDB().getFingerPrints().size());

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
