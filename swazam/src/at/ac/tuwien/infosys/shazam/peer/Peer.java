package at.ac.tuwien.infosys.shazam.peer;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.logging.Logger;

import at.ac.tuwien.infosys.shazam.communication.listener.MessageHandler;
import at.ac.tuwien.infosys.shazam.communication.listener.SocketListener;
import at.ac.tuwien.infosys.shazam.peer.commands.HandleListOfPeersFromServerCommand;
import at.ac.tuwien.infosys.shazam.peer.commands.HandleServerRequestCommand;

public class Peer implements Runnable {

	private String userName;
	private String password;
	private String ip;
	private int port;
	private SampleDatabase sampleDB;
	private ArrayList<String> knownPeers;
	private static Logger log;

	public static final String SERVER_IP = "localhost";
	public static final int SERVER_PORT = 12345;

	private static MessageHandler mh;

	public Peer() {
		super();
		knownPeers = new ArrayList<String>();
		sampleDB = new SampleDatabase();

	}

	public Peer(String userName, String password, String ip, int port) {
		super();

		this.userName = userName;
		this.password = password;
		this.ip = ip;
		this.port = port;
		knownPeers = new ArrayList<String>();
		sampleDB = new SampleDatabase();
		log = Logger.getAnonymousLogger();
	}

	@Override
	public void run() {

		// Connection to the server will be established
		Socket clientSocket = null;
		try {
			clientSocket = new Socket(SERVER_IP, SERVER_PORT);
		} catch (UnknownHostException e) {
			log.info("UnknownHostException");
			e.printStackTrace();
		} catch (IOException e) {
			log.info("IOException");
			e.printStackTrace();
		}
		// Request to join the network
		// 1;user;pass;192.168.1.1:5432
		PrintWriter outToServer = null;
		try {
			outToServer = new PrintWriter(clientSocket.getOutputStream(), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
		// log.info("Requesting joining the network");
		outToServer.println("1;" + this.userName + ";" + this.password + ";" + this.ip + ":" + this.port);

		mh = new MessageHandler();

		// Getting a list of peers known by the Server
		// 2;192.168.1.2:4325; 192.168.1.21:5432;...
		mh.registerMessageHandler(2, new HandleListOfPeersFromServerCommand(this));
		// 5;HASH;3;SERVERIP:SERVERPORT
		// find hash, timeout, server_ip, port
		mh.registerMessageHandler(5, new HandleServerRequestCommand(this));
		// initialize socket listener
		SocketListener listener = new SocketListener(ip, port, mh);

		try {
			new Thread(listener).start();
		} catch (Exception e) {
			log.warning("Could not create SocketListener on port " + port + "\n" + e);
			e.printStackTrace();
		}
		// log.info("Shutting down server...");

	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public ArrayList<String> getKnownPeers() {
		return knownPeers;
	}

	public void setKnownPeers(ArrayList<String> knownPeers) {
		this.knownPeers = knownPeers;
	}

	public SampleDatabase getSampleDB() {
		return sampleDB;
	}

	public void setSampleDB(SampleDatabase sampleDB) {
		this.sampleDB = sampleDB;
	}

	public static void main(String[] args) {

		Peer p;

		if (args.length < 2) {
			System.out.println("Error : Argument number");
			return;
		}

		p = new Peer();
		p.setIp("localhost");
		p.setPort(Integer.valueOf(args[0]));
		System.out.println("A new peer with the name that listen to the port " + args[0] + "has been generated");

		for (int i = 1; i < args.length; i++) {
			System.out.println("A sample with the path " + args[i] + " will be added to this peer");
			p.getSampleDB().add(args[i]);
			System.out.println("The sample with the name " + args[i].split("/")[args[i].split("/").length - 1]
					+ " has been added to the list of known samples");

		}
		// Thread x = new Thread(p);
		p.run();

	}

}
