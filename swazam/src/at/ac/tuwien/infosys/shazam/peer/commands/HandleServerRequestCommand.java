package at.ac.tuwien.infosys.shazam.peer.commands;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import at.ac.tuwien.infosys.shazam.communication.listener.Command;
import at.ac.tuwien.infosys.shazam.peer.Peer;
import at.ac.tuwien.infosys.swa.audio.Fingerprint;
import at.ac.tuwien.infosys.swa.audio.SubFingerprint;

//5;HASH;3;SERVERIP:SERVERPORT
public class HandleServerRequestCommand implements Command {
	private Peer peer;

	public HandleServerRequestCommand(Peer peer) {
		super();
		this.peer = peer;
	}

	// 5;3;SERVERIP:SERVERPORT;ID;shift;subFingerprint1;subFingerPrint2;....
	// find ,timeout,server_ip,shiftDuration,subprint1,subprint2,subperint3
	@Override
	public void execute(String[] msg, String fromHost, int fromPort) {

		PrintWriter outToServer = null;
		String result;
		Socket clientSocket = null;
		// If a match was found then send the result to the server else forward
		// the request to the other peers known by this one

		double shift = Double.valueOf(msg[4]);
		ArrayList<SubFingerprint> fps = new ArrayList<SubFingerprint>();
		for (int i = 5; i < msg.length; i++)
			fps.add(new SubFingerprint(Integer.valueOf(msg[i])));

		Fingerprint fp = new Fingerprint(0.0, shift, fps);

		if ((result = this.peer.getSampleDB().find(fp)) != null) {
			System.out.println("Match was found by the peer " + peer.getUserName());
			System.out.println("Result will be send to the server");
			try {
				clientSocket = new Socket(msg[2].split(":")[0], Integer.parseInt(msg[2].split(":")[1]));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				outToServer = new PrintWriter(clientSocket.getOutputStream(), true);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// 6;user;pass;HASH;RESULT
			outToServer.println("6;" + this.peer.getUserName() + ";" + this.peer.getPassword() + ";" + msg[3] + ";" + result);
		}
		// No Match is found
		else {
			int hop;
			if ((hop = (Integer.parseInt(msg[1]) - 1)) == 0)
				return;

			String messageToPeer = "5;" + hop;
			for (int i = 2; i < msg.length; i++)
				messageToPeer = messageToPeer + ";" + msg[i];

			for (String peer : this.peer.getKnownPeers()) {

				try {
					clientSocket = new Socket(peer.split(":")[0], Integer.parseInt(peer.split(":")[1]));
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch (UnknownHostException e) {
					//System.out.println("1 The peer with the name "+ peer +" was not found");
					e.printStackTrace();
				} catch (IOException e) {
					System.out.println("2 The peer with the name "+ peer +" was not found");
					e.printStackTrace();
				}
				try {
					outToServer = new PrintWriter(clientSocket.getOutputStream(), true);
				} catch (IOException e) {
					//System.out.println("3 The peer with the name "+ peer +" was not found");
					e.printStackTrace();
				}
				// 5;3;SERVERIP:SERVERPORT;ID;shift;subFingerprint1;subFingerPrint2;....
				// find hash, timeout, server_ip

				outToServer.println(messageToPeer);
			}
		}
	}

}
