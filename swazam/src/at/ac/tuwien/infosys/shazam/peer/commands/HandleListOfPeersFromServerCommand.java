package at.ac.tuwien.infosys.shazam.peer.commands;

import java.util.ArrayList;

import java.util.ArrayList;

import at.ac.tuwien.infosys.shazam.communication.listener.Command;
import at.ac.tuwien.infosys.shazam.peer.Peer;

//2;192.168.1.2:4325;	192.168.1.21:5432;...
public class HandleListOfPeersFromServerCommand implements Command {
	private Peer peer;

	public HandleListOfPeersFromServerCommand(Peer peer) {
		super();
		this.peer = peer;
	}

	@Override
	public void execute(String[] msg, String fromHost, int fromPort) {

		ArrayList<String> peers = new ArrayList<String>();

		for (int i = 1; i < msg.length; i++) {
			if (msg[i].split(":")[1].equals(Integer.toString(this.peer.getPort()))) {
				//peers.add(msg[i]);
				System.out.println("Port of added peer :" + msg[i]);
				//System.out.println("do not add yourself to the list of known peers");
			}
			else{
				peers.add(msg[i]);
				System.out.println(msg[i]);
				//System.out.println("Peer Port " + this.peer.getPort() +" and the received peer" + msg[i].split(":")[1]);
				
			}
		}
		this.peer.setKnownPeers(peers);

	}
}