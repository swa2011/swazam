package at.ac.tuwien.infosys.shazam.peer;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import at.ac.tuwien.infosys.swa.audio.Fingerprint;
import at.ac.tuwien.infosys.swa.audio.FingerprintSystem;
import at.ac.tuwien.infosys.swa.audio.SubFingerprint;

public class SampleDatabase {

	private Logger log;
	//Name of the song, Fingerprint
	private Map<Fingerprint,String> fingerPrints;

	public SampleDatabase() {
		super();
		this.log = Logger.getAnonymousLogger();
		fingerPrints = new HashMap<Fingerprint,String>();
	}

	// Add a new Sample to the peer database
	public void add(String path) {

		String fileName = path.split("/")[path.split("/").length - 1];
		Fingerprint f = null;
		File inputFile = new File(path);

		try {
			AudioInputStream in = AudioSystem.getAudioInputStream(inputFile);
			log.info("Start generating fingerprint");
			f = FingerprintSystem.fingerprint(in);
			log.info("Fingerprint generated");
			fingerPrints.put(f,fileName);

		} catch (UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// Get all names of samples known by this peer
	public void getKnownSamples() {

		for (Fingerprint fp : this.fingerPrints.keySet())
			System.out.println(fingerPrints.get(fp));

	}

	// find a sample that matches the specified fingerprint
	public String find(Fingerprint fp) {

		
		
		Fingerprint toReturn = null;
		for (Fingerprint f : this.fingerPrints.keySet()) {
			if ( f.match(fp) >= 0.0) {
				toReturn = f;
				break;
			}

		}

		if (toReturn == null)
			return null;
				
		return fingerPrints.get(toReturn);
	}

	public Map<Fingerprint,String> getFingerPrints() {
		return fingerPrints;
	}

	public void setFingerPrints(Map<Fingerprint,String> fingerPrints) {
		this.fingerPrints = fingerPrints;
	}

}
