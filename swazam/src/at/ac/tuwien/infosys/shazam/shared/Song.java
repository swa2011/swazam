package at.ac.tuwien.infosys.shazam.shared;

import at.ac.tuwien.infosys.swa.audio.Fingerprint;
/**
 * Container class for information regarding a song
 * @author david
 *
 */
public class Song {
	
	private String artist;
	private String title;
	private String album;
	private String localPath;
	private Fingerprint fingerprint = null;
	private int id = -1;
	/**
	 * creates a song with empty fields
	 */
	public Song(){
		artist ="";
		title = "";
		album ="";
		localPath ="";
	}
	
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAlbum() {
		return album;
	}
	public void setAlbum(String album) {
		this.album = album;
	}
	public String getLocalPath() {
		return localPath;
	}
	public void setLocalPath(String localPath) {
		this.localPath = localPath;
	}
	public Fingerprint getFingerprint() {
		return fingerprint;
	}
	public void setFingerprint(Fingerprint fingerprint) {
		this.fingerprint = fingerprint;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
}
