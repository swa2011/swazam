package at.ac.tuwien.infosys.shazam.communication.listener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.logging.Logger;


public class WorkerRunnable implements Runnable{

    protected Socket clientSocket = null;
    protected MessageHandler mh;
    Logger log = Logger.getAnonymousLogger();
    
    public WorkerRunnable(Socket clientSocket, MessageHandler mh) {
        this.clientSocket = clientSocket;
        this.mh = mh;
    }

    public void run() {
        try {
        	BufferedReader input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            mh.processMessage(input.readLine(), clientSocket.getInetAddress().getHostAddress(), clientSocket.getPort());
            clientSocket.close();
            input.close();
        } catch (IOException e) {
            log.warning("Error reading from Socket\n" + e);
        }
    }
}