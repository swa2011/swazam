package at.ac.tuwien.infosys.shazam.communication.listener;

public interface Command { 

	/**
	 * 
	 * @param msg msg the message to process, split at ";"
	 * @param fromHost the IP address of the sender
	 * @param fromPort the port of the sender
	 */
	public void execute(String[] msg, String fromHost, int fromPort);
	 
}

