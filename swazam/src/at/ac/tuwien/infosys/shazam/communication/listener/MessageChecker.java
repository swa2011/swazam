package at.ac.tuwien.infosys.shazam.communication.listener;

import java.util.logging.Logger;

public class MessageChecker {
	private Logger log = Logger.getAnonymousLogger();
	
	/**
	 * 
	 * @param msg a message split by ";"
	 * @return true if message format is valid, false if not
	 */
	public boolean checkMessage(String[] msg) {
		boolean result = true;
		result &= checkMessageType(msg);
		result &= checkMessageLength(msg);
		return result;
	}
	
	/**
	 * 
	 * @param msg a message split by ";"
	 * @return true if message has valid message type
	 */
	public boolean checkMessageType(String[] msg) {
		try {
			Integer.parseInt(msg[0]);			
		}
		catch (NumberFormatException e) {
			log.warning("Message type not a number\n" + e);
			return false;
		}
		return true;
	}
	
	/**
	 * 
	 * @param msg a message split by ";"
	 * @return true if message has the right number of fields, else otherwise
	 */
	public boolean checkMessageLength(String[] msg) {
		
		return true;
		
		// TODO rewrite length tests
//		int msgLength = msg.length;
//		int expectedLength = -1;
//		switch(Integer.parseInt(msg[0])) {
//			case 1:
//				expectedLength = 4;
//				break;
//			case 3:
//				expectedLength = 2;
//				break;
//			case 6:
//				expectedLength = 5;
//				break;
//			case 10:
//				expectedLength = 4;
//				break;
//			case 12:
//				expectedLength = 3;
//				break;
//		}
//		
//		return expectedLength == msgLength;
	}
	
}
