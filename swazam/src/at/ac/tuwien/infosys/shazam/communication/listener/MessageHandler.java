package at.ac.tuwien.infosys.shazam.communication.listener;

import java.util.HashMap;
import java.util.logging.Logger;


public class MessageHandler {

	private Logger log;
	private HashMap<Integer,Command> map;
	private MessageChecker mc;
    
	public MessageHandler() {
		log = Logger.getAnonymousLogger();		
		log.info("Starting Message Handler...");
		map = new HashMap<Integer, Command>();
		mc = new MessageChecker();
	}

	/**
	 * takes a message, splits it and calls further methods accordingly
	 * @param msg a message as reveived from somewhere
	 * @param fromHost the senders IP address
	 * @param fromPort the senders port
	 */
	public void processMessage(String msg, String fromHost, int fromPort) {
		int msgType = 0;
		String[] splitMsg;
		splitMsg = msg.split(";");		
		log.info("processing message \"" + msg + "\" from "+ fromHost + ":" + fromPort);
		
		if (!mc.checkMessage(splitMsg)) {
			log.warning("Message format not correct");
			return;
		}

		// execute registered command
		msgType = Integer.parseInt(splitMsg[0]);			

		if (map.containsKey(msgType)) {
			map.get(msgType).execute(splitMsg, fromHost, fromPort);			
		}
	}
	
	/**
	 * register a messagehandler for a specific message type
	 * @param messageType the type of the message
	 * @param handler the command that handles that message type
	 */
	public void registerMessageHandler(int messageType, Command handler) {
		map.put((Integer)messageType, handler);
	}
}