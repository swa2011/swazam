package at.ac.tuwien.infosys.shazam.communication.dispatcher;

import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Logger;

public class MessageDispatcher {

	private Logger log = Logger.getAnonymousLogger();

	/**
	 * send a message to a host
	 * @param host receivers IP
	 * @param port receivers port
	 * @param message message to send
	 */
	public void dispatchMessage(String host, int port, String message) {
		// TODO maybe enable the message checker again, but would need some changes there
		// mc.checkMessage(message.split(";"));
		
		try {
			Socket clientSocket = new Socket(host, port);
			PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
			out.println(message);
			out.close();
			clientSocket.close();
			log.info("Sent message " + message + " to " + host + ":" + port);

		} catch (Exception e) {
			log.warning("Could not send message " + message + ".\n" + e);
		}
	}
}
