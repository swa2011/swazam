package at.ac.tuwien.infosys.shazam.communication.listener;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

public class SocketListener implements Runnable{
	
	private String host;
	private int port;
	private MessageHandler mh;
	private ServerSocket serverSocket = null;
    private boolean isStopped = false;
	protected Thread runningThread = null;
    
	Logger log;
	
	/**
	 * Constructor
	 * @param host the IP of the system running the listener
	 * @param port the port to listen to
	 * @param mh the MessageHandler used by the listener
	 */
	public SocketListener(String host, int port, MessageHandler mh) {
		log = Logger.getAnonymousLogger();		
		log.info("Starting Socket Listener...");

		this.host = host;
		this.port = port;
		this.mh = mh;
 	}

	public void run() {
	    synchronized(this){
            this.runningThread = Thread.currentThread();
        }
		try {
			log.info("Opening socket on " + host + ":" + port); 
			openServerSocket();
			while(!isStopped()){
				Socket clientSocket = null;
				try {
					clientSocket = this.serverSocket.accept();
				} catch (IOException e) {
					if(isStopped()) {
						return;
					}
					log.warning("Error accepting client connection");
				}
				new Thread(
						new WorkerRunnable(
								clientSocket, mh)
						).start();
			}
			log.info("Server Stopped.");
		} catch (Exception e) {
			log.warning("Could not listen on port: " + host + ":" + port);
			System.exit(1);
		}
	}	
	
    private synchronized boolean isStopped() {
        return this.isStopped;
    }
	
	public synchronized void stop(){
		this.isStopped = true;
		try {
			this.serverSocket.close();
		} catch (IOException e) {
			throw new RuntimeException("Error closing server", e);
		}
	}

	private void openServerSocket() {
		try {
			this.serverSocket = new ServerSocket(port);
		} catch (IOException e) {
			log.warning("Cannot open port " + port);
		}
	}
}
