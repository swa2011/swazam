<?xml version="1.0" encoding="UTF-8" ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="at.ac.tuwien.infosys.shazam.server.AccountDatabase"%>
<%@page import="at.ac.tuwien.infosys.shazam.server.*"%>
<html>
<title>JSP Demo</title>
<body bgcolor="blue" text="white">

<!-- DIRECTIVES -->
<%@ page language = "java" %>
<%@ page import = "java.util.*" %>
<%@ page import = "at.ac.tuwien.infosys.*" %>

<%@ page contentType = "TEXT/HTML" %>

<!-- SCRIPLETS -->
<H1>
<% if (Calendar.getInstance().get(Calendar.AM_PM) == Calendar.AM)
{ %>
Good Morning ! 
<% } else { %>
Good Afternoon! 
<% } %>
</H1>

<H2> If you're seeing this page via a web browser, it means you've integrated Tomcat and Eclipse successfully. Congratulations!</H2>
<jsp:useBean id="server" class="at.ac.tuwien.infosys.shazam.server.Server" scope="application" />

<% 
out.println(server.lol()); %>


</body>
</html>