<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="at.ac.tuwien.infosys.shazam.server.Account"%>
<%@page import="at.ac.tuwien.infosys.shazam.server.AccountDatabase"%>
<%@ page language = "java" %>
<jsp:useBean id="accDB" class="at.ac.tuwien.infosys.shazam.server.AccountDatabase" scope="session" />

<html>
	<head><title>Using Post Method in JSP Form.</title></head>
	<body>
		<form method="post">
					Username: <input type="text" size="20" name="username" />
					Password: <input type="text" size="20" name="pass1" />
					Password: <input type="text" size="20" name="pass2" />
					email: <input type="text" size="20" name="email" />
					
					
<input type="submit" name="B1" value="Submit" />
<input type="reset" name="B2" value="Reset" />
		</form>
		<%
		
		String username = request.getParameter("username");
		String pass1 = request.getParameter("pass1");
		String pass2 = request.getParameter("pass2");
		String email = request.getParameter("email");
		
			if(username != null) {
				if(request.getParameter("username").equals("")) {
					out.println("<font color=red>Please enter your name.</font>");	
				} else if (! pass1.equals(pass2)) {
					out.println("<font color=red>Your passwords don't match.</font>");	
				} else if(email.equals("")) {
					out.println("<font color=red>Please enter your email adress.</font>");	
				}
				else {
					//Account acc = new Account(request.getParameter("username"),request.getParameter("password"));
					Account acc = new Account(username, pass1);
					acc.setEmail(email);
					accDB.add(acc);
					accDB.save();
					out.println("Your user " + request.getParameter("username") + " has been created");
					
					//out.println(accDB.toString());
				}
			}
		%>
	</body>
</html>
