<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="at.ac.tuwien.infosys.shazam.server.Account"%>
<%@page import="at.ac.tuwien.infosys.shazam.server.AccountDatabase"%>
<%@ page language = "java" %>
<jsp:useBean id="accDB" class="at.ac.tuwien.infosys.shazam.server.AccountDatabase" scope="session" />

<html>
	<head><title>Using Post Method in JSP Form.</title></head>
	<body>
	<table>
	<thead><tr>
		<td>ID</td>
	<td>Username</td>
	<td>email</td>
	<td>coins</td>
	</tr>
	</thead>
	<%
		for (Account a : accDB.getAccounts()) {
			out.println("<tr>");
			out.println("<td>" + a.getId() + "</td>");
			out.println("<td>" + a.getUsername() + "</td>");
			out.println("<td>" + a.getEmail() + "</td>");
			out.println("<td>" + a.getCoins() + "</td>");
			out.println("</tr>");
		}
	%>
		</table>
	</body>
</html>