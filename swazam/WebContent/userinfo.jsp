<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="at.ac.tuwien.infosys.shazam.server.*"%>

<%@ page language = "java" %>
<jsp:useBean id="accDB" class="at.ac.tuwien.infosys.shazam.server.AccountDatabase" scope="session" />
<jsp:useBean id="searchDB" class="at.ac.tuwien.infosys.shazam.server.SearchDatabase" scope="session" />

<html>
	<head><title>Using Post Method in JSP Form.</title></head>
	<body>
		<form method="post">
					Username: <input type="text" size="20" name="username" />
					Password: <input type="text" size="20" name="pass" />

					
					
<input type="submit" name="B1" value="Submit" />
<input type="reset" name="B2" value="Reset" />
		</form>
		<%
		
		String username = request.getParameter("username");
		String pass = request.getParameter("pass");

		
			if(username != null) {
				Account user = accDB.find(username,pass);
				
				if (user == null) {
					user = accDB.find(username);
					if (user == null) {
						out.println("<font color='red'>User not found</font>");
					} else {
						out.println("<font color='red'>Username or Password wrong</font>");
					}
					return;
				}
				
				out.println(username + " " + user.getEmail() + " Coins:" + user.getCoins() + "<br/><br/>");

				 %>
				 <table>
	<thead><tr>
	<td>Date</td>
	<td>found</td>
	<td>by</td>
	<td>result</td>
	</tr>
	</thead>

	<%
	
				for(SearchRequest r : searchDB.findByUser(username)) {

			out.println("<tr>");
			out.println("<td>" + r.getDate() + "</td>");
			out.println("<td>" + r.isFound() + "</td>");
			out.println("<td>" + r.getFoundBy() + "</td>");
			out.println("<td>" + r.getResult() + "</td>");
			out.println("</tr>");

				}
	%>
		</table> <%

			}
		%>
	</body>
</html>
