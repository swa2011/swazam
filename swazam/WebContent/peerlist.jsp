<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="at.ac.tuwien.infosys.shazam.server.*"%>
<%@ page language = "java" %>
<jsp:useBean id="peerDB" class="at.ac.tuwien.infosys.shazam.server.PeerDatabase" scope="session" />

<html>
	<head><title>Using Post Method in JSP Form.</title></head>
	<body>
	<table>
	<thead><tr>
	<td>Username</td>
	<td>IP</td>
	<td>Port</td>
	</tr>
	</thead>

	<%
		for (Peer p : peerDB.getPeerList()) {
			out.println("<tr>");
			out.println("<td>" + p.getUsername() + "</td>");
			out.println("<td>" + p.getIp() + "</td>");
			out.println("<td>" + p.getPort() + "</td>");
			out.println("</tr>");
		}
	%>
		</table>
	</body>
</html>